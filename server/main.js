const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');
const cors = require('cors');

const mysqlConfig = require('./mysql-config');
const pool = mysql.createPool(mysqlConfig);

const app = express();
app.use(cors());

//Routes
const SELECT_BOOKS = 'select id, title, author_lastname, author_firstname from books limit ?';

app.get('/books', (req, resp) => {

    const limit = parseInt(req.query.limit) || 10;
    

    pool.getConnection((error, conn) =>{
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(SELECT_BOOKS, [limit],
            (error, results) =>{
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results);
                } finally {
                    conn.release();
                }
            })
    })

})


const SELECT_BY_BOOK_TITLE = 'select * from books where id = ?';

app.get('/books/:id', (req, resp) => {
    console.log(`id = ${req.params.id}`);

    pool.getConnection((error, conn) => {
       if (error) {
           resp.status(500).json({error: error}); return;
       }
       conn.query(SELECT_BY_BOOK_TITLE, [ req.params.id],
           (error, result) => {
               try {
                   if (error) {
                       resp.status(400).json({error: error}); 
                       return;
                   }
                   if (result.length)
                       resp.status(200).json(result[0]);
                   else
                       resp.status(404).json({error: 'Not Found'});
               } finally { conn.release() ;}
           }
       )
   })
});

const SELECT_BY_BOOK_FIRST = 'select * from books where author_firstname = ?';

app.get(['/books/:about/:author_firstname'], (req, resp) => {
    console.log(`author_firstname = ${req.params.author_firstname}`);

    pool.getConnection((error, conn) => {
       if (error) {
           resp.status(500).json({error: error}); return;
       }
       conn.query(SELECT_BY_BOOK_FIRST, [ req.params.author_firstname],
           (error, result) => {
               try {
                   if (error) {
                       resp.status(400).json({error: error}); 
                       return;
                   }
                   if (result.length)
                       resp.status(200).json(result[0]);
                   else
                       resp.status(404).json({error: 'Not Found'});
               } finally { conn.release() ;}
           }
       )
   })
});


const PORT = process.argv[2] || process.env.APP_PORT || 3000;
app.listen(PORT, () => {
    console.log('Application started on port %d', PORT);
});