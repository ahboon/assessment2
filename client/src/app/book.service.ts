import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class BookService {

    readonly SERVER = 'http://localhost:3000'

    cursor = 0;
  
    constructor(private httpClient: HttpClient) { }
  
    // GET /film/<filmId>
    getbook(id: number): Promise<any> {
      return (
        this.httpClient.get(`${this.SERVER}/books/${id}`)
          .take(1).toPromise()
      );
    }
  
    // GET /films?limit=<n>&offset=<n>
    getAllBooks(config = {}): Promise<any> {
      //Set the query string
      let qs = new HttpParams();
      qs = qs.set('limit', config['limit'] || 10)
        
      return (
        this.httpClient.get(`${this.SERVER}/books`, { params: qs })
          .take(1).toPromise()
      );
    }
}
 



