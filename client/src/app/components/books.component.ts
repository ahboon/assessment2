import { Component, OnInit, ViewChild } from '@angular/core';
import { BookService } from '../book.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-films',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  @ViewChild(NgForm) customerForm: NgForm;

  offset = 0;
  private limit = 10;
  books = [];

  constructor(private bookSvc: BookService, private router: Router,
    private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
      console.log('in ngOnInit')
      this.offset = this.bookSvc.cursor;
      this.loadbook();
    }
  
    private loadbook() {
      this.bookSvc.getAllBooks({limit: this.limit })
        .then((result) => { this.books = result })
        .catch((error) => { console.error(error); });
    }
  
    getAllBook(_libraryForm: NgForm) {

      this.bookSvc.getAllBooks({})
        .then(result => {
          console.log('>>> result: ', result);
          this.books = result
        })
        .catch(error => {
          console.log('error: ', error);
        })
    }
  
    showDetails(id: number) {
      console.log('> id: %d', id);
      this.router.navigate([ '/books', id ]);
    }
}
