import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BookService } from '../book.service';
import { Subscription } from 'rxjs/subscription';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit, OnDestroy {

  book = {};
  id = 0;

  private id$: Subscription;
  
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private bookSvc: BookService) { }

  ngOnInit() {
    console.log("BookDetail ngOnInit: ");
    this.id$ = (this.activatedRoute.params.subscribe(
      (param) => {
        console.log('> param =', param);
        this.id = parseInt(param.id);
        this.bookSvc.getbook(this.id)
          .then((result) => this.book = result)
          .catch((error) => {
            alert(`Error: ${JSON.stringify(error)}`)
          });
      }
    ));
    
  }

  ngOnDestroy(){
    this.id$.unsubscribe();
  }

  goBack() {
    this.router.navigate(['/']);
  }

}
